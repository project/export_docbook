Export_DocBook module
=====================

Description:
------------
        
This module allows the export of Drupal books, in DocBook
XML format.  It is assumed that the input is HTML; this
input is cleaned using Tidy to generate XHTML, which is
then transformed via XSLT into DocBook.

Version:
________

This version of export_docbook depends on book.module
v1.334 or later (CVS HEAD on Nov 30, 2005)


Pre-requisites:
_______________

The module assumes that you are running PHP5, and have
Tidy support enabled.


Bugs:
_____

The way in which the generated XML is supplied to the user is
unfriendly.  Perhaps a better approach would be to take the
user to a page with a generated link to download the 
requested XML.

The output transformation is not terribly well tested yet.
It's not complete, though it should cover most of the 
commonly used HTML constructs. 

Please report bugs via Drupal's issue tracker: 
http://drupal.org/project/issues


Credits
-------

The XSLT script is based on HTML2DocBook, by Jeff Beal, as found
on wiki.docbook.org.


History
-------

2005-07-22: First (alpha) release.
2005-11-22: Beta release.


Author
-------

Djun Kim (puregin@puregin.org)

